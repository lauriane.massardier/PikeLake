package pikelake.environment;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe qui lit le fichier contenant le marnage du lac
 *		Ces valeurs sont rangees a l'initialisation dans un tableau pour un acces plus rapide
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class FichierMarnage  extends AquaNismsGroupProcess<Pike,PikesGroup> {
	
	// Tableau contenant le marnage en fonction de la date
	public static String dateMarnage[][][] = new String[32][13][24];	//	[jour]	[mois]	[heure]
	
	/**
	 *		Lis le fichier contenant le marnage du lac
	 *		Remplis le tableau dateMarnage
	 *		@param object L'ensemble des individus
	 *		@return /
	 */
	@Override
	public void doProcess (PikesGroup object) {
		
		// Fichier contenant le marnage du lac pour chaque heure dans l'annee
    	String file = "data/input/CoteCorrige_01012012_30042014.txt";
    	try {
    		Scanner scanner = new Scanner(new File(file));
    		String line = scanner.nextLine();
	    	
	    	// On boucle sur chaque ligne detect�e
	    	while (scanner.hasNextLine()) {
	    		
	    		line = scanner.nextLine();				// Lecture de la ligne
	    		String temp[] = line.split("[/:\t ]+");	// Decoupage de la ligne

	    		// Initialisation du tableau comprenant le marnage pour chaque date (mois, jour, heure)
	    		dateMarnage[Integer.parseInt(temp[0])] [Integer.parseInt(temp[1])] [Integer.parseInt(temp[3])] = 
	    									temp[6].substring(0, temp[6].length()-1).replace(".", "_");	
	    	}
	       	scanner.close(); // Fermeture du fichier
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, erreur de lecture du fichier
			e.printStackTrace();
		}
	}	
	
	/**
	 *		Retourne le marnage fonction d'une date (jour+mois+heure)
	 *		@param jour Entier contenant le jour
	 *		@param mois Entier contenant le mois
	 *		@param heure Entier contenant l'heure
	 *		@return dateMarnage[jour][mois+1][heure] Chaine de caract�re contenant le marnage
	 */
	public static String calculMarnage (int jour, int mois, int heure) {
		return dateMarnage[jour][mois][heure];
	}
}