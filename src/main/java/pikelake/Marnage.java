package pikelake;

import java.io.File;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.FileNotFoundException;

import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;
import pikelake.environment.Time;
import pikelake.environment.FichierMarnage;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe qui met a jour les cellules accessibles du lac, ainsi que leurs HSI
 *		en fonction du marnage horaire donne par la classe FichierMarnage
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class Marnage extends AquaNismsGroupProcess<Pike,PikesGroup> {

	public static String marnageOld = null, marnageNew = null;

	/**
	 *		Teste si la cote du lac est modifiee, si oui
	 *		MAJ de la grille et du du HSI de chaque cellule 
	 *		@param object Represente les individus
	 *		@return /
	 */	
	@Override
	public void doProcess(PikesGroup object) {
		// Recuperation de la grille
		Grid grid = (Grid) pilot.getAquaticWorld().getEnvironment();
		grid = grid.getGrid();
		// Calcul du marnage
		marnageNew = FichierMarnage.dateMarnage[Time.jourMois][Time.mois][Time.heure];
		// MAJ de la grille seulement si le marnage est modifie
		if (!marnageNew.equals(marnageOld)) {
                    synchronized (grid) { // synchronization to be sure that graphical observer read synchronized values
                        majCote(grid);
                    }
                }
	}

	/**
	 *		MAJ de la grille, augmentation ou diminution du nombre de cellules accessibles
	 *		Modification du HSI de chaque cellule
	 *		@param grid Represente le lac et les cellules non accessibles
	 *		@return /
	 */
    public void majCote (Grid grid) {
		
    	StringTokenizer sLigne;
	    @SuppressWarnings("unused")
		double hsiStd = 0, hsiMoy = 0;
		int idCell = 0, yPike = 0, xPike = 0;
		marnageOld = marnageNew;
		
	    // Initialisation de toute la grille avec hsi = -1
	    for (int cptCell = 0; cptCell < (grid.getGridWidth() * grid.getGridHeight() - 1); cptCell++)
	    	grid.setCell(cptCell, -1);
	    
	    // Lecture fichier contenant les HSI de toutes les mailles
	    String filePath = "data/input/HSI/hsi_BRO" + Time.getSeason() + marnageNew + ".txt";
            System.out.println(filePath);
	    Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
		   	// On boucle sur chaque ligne detect�e
		   	while (scanner.hasNextLine()) {
		   		line = scanner.nextLine();
		   		
		   		// Decoupage ligne : id, hsiStd, hsiMoy
		   		sLigne = new StringTokenizer (line);
		   		if (sLigne.hasMoreTokens())
		   			idCell = Integer.parseInt(sLigne.nextToken())-1;
		   		if (sLigne.hasMoreTokens())
		    	   	hsiStd = Double.parseDouble(sLigne.nextToken());
		   		if (sLigne.hasMoreTokens())
		    	   	hsiMoy = Double.parseDouble(sLigne.nextToken());
		   		
		   		// Conversion idCell en coordonnees (x, y) 
		   		// (x, y) avec les id de cellules de 0 � n-1
		   		yPike = (int) Math.floor(idCell / grid.getGridWidth());
		       	xPike = (int) idCell - (yPike * grid.getGridWidth());
		       	
		       	// Inversion des coordonnees en Y (place l'origine en bas � gauche)
		       	yPike = (grid.getGridHeight()-1) - yPike ;
		       	
		       	// Conversion des coordonnees (x, y) en idCell 
		       	idCell = xPike + yPike * grid.getGridWidth();
		       	
		       	// Initialisation du hsi de la cellule(idCell)
		       	grid.setCell(idCell, hsiMoy);
		   	}
		   	scanner.close(); // Fermeture du fichier
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, erreur de lecture du fichier
			e.printStackTrace();
		}
	}
}

