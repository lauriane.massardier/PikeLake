package pikelake;

import java.util.List;
import java.util.ArrayList;

import pikelake.pikes.Pike;

import fr.cemagref.simaqualife.extensions.spatial2D.IndexedCell;


public class Cell extends IndexedCell {

    /**
     * <code>habitatQuality</code> The quality of this place (pike point of
     * view). A high value means that preys are vulnerable for the pikes
     */
    private double habitatQuality;

    /**
     * <code>pikes</code> The list of pikes in this place
     */
    private transient List<Pike> pikes;

    public Cell (int index, double habitatQuality) {
        super(index);
        this.habitatQuality = habitatQuality;

        pikes = new ArrayList<Pike>();
    }

    
    public List<Pike> getPikes () {
        return pikes;
    }

    
    public boolean addPike (Pike pike) {
        return pikes.add(pike);
    }

    
    public boolean removePike (Pike pike) {
        return pikes.remove(pike);
    }

    
    public double getHabitatQuality () {
        return habitatQuality;
    }

}
