package pikelake.pikes;

import java.util.ArrayList;
import java.util.List;

import umontreal.iro.lecuyer.probdist.PoissonDist;
import umontreal.iro.lecuyer.randvar.PoissonGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 *
 */
public class PikeReproductionProcess extends AquaNismsGroupProcess<Pike,PikesGroup> {
	
	/**
	 * <code>ageAtFirstReproduction</code> age at first reproduction (in year)
	 */
	private int ageAtFirstReproduction = 5;
	
	/**
	 * <code>pikeFertility</code> mean number of offsprings per pike
	 */
	private double pikeFertility = 1.1;
	
	transient private PoissonGen poissonGen;

	public void initTransientParameters (Pilot pilot) {
		poissonGen = new PoissonGen(pilot.getRandomStream(), new PoissonDist(pikeFertility));
		
	}
	
	public void doProcess (PikesGroup group) {
		System.out.println("PikeReproductionProcess");
		
		int offspring;
		if (1+ ((group.getPilot().getCurrentTime()-1) % 12) == group.getMonthOfBirth()){
			List<Pike> offsprings = new ArrayList<Pike>();
			List<Pike> deadSpwaners = new ArrayList<Pike>();
			for (Pike pike: group.getAquaNismsList()){
				if (Math.floor((double) pike.getAge()/12.) >= ageAtFirstReproduction){
					// number of offspring
					offspring = poissonGen.nextInt();

					System.out.println("  offspring #: "+ offspring);
					for (int i=0; i<offspring ;i++)
						offsprings.add(new Pike(group.getPilot(), pike.getPosition()));
					// die after reproduction
					//if (offspring > 0) // only if reproduce
						deadSpwaners.add(pike);		
				}	
			}
	    // update the pikesGroup
		for (Pike pike: deadSpwaners){
			group.getAquaNismsList().remove(pike);
			//ASK WHY
			pike.getPosition().removePike(pike);
		}

		for (Pike pike: offsprings)
			group.addAquaNism(pike);
		}
	}
	
}
