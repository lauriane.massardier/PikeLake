
Ligne de commande :

	-d -simDuration 8760 -observers data/input/observersCharts.xml -groups data/input/fishLight.xml -env data/input/grid.xml

					-d				: mode d�bug (affiche message erreurs, avertissements...)
					-simDuration	: nombre de pas de la simulation (8760 = 1an pour un pas horaire)
					-observers		: fichier d'initialisation des observeurs
					-groups			: fichier d'initialisation des groupes et process
					-env			: fichier d'initialisation de l'environnement



	Entr�es

		- Marnage
		- HSI (marnage, saison)
		- Rayon max d�placement (saison, phase jour)
		- horaires du soleil

	Sorties

		- D�placements de chaque individus (historique : Pas de temps, date, id_maille par individu)



	Process

		Initialisation :
		
			HoraireLeverCoucher
			FichierMarnage
			AreaMovement
			PikesPopulateProcess
		
		Chaque pas de temps :

			Time
			Marnage
			PikeMovement
			PikeTrackLocation
			
		Fin du programme :
		
			SaveLocation
	